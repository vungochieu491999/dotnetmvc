﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartManagement.Models;

namespace CartManagement.Controllers
{
    public class PRODUCT_TYPEController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();

        // GET: PRODUCT_TYPE
        public ActionResult Index()
        {
            return View(db.PRODUCT_TYPE.ToList());
        }

        // GET: PRODUCT_TYPE/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            if (pRODUCT_TYPE == null)
            {
                return HttpNotFound();
            }
            return View(pRODUCT_TYPE);
        }

        // GET: PRODUCT_TYPE/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PRODUCT_TYPE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PRODUCT_TYPE_CD,NAME")] PRODUCT_TYPE pRODUCT_TYPE)
        {
            if (ModelState.IsValid)
            {
                db.PRODUCT_TYPE.Add(pRODUCT_TYPE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pRODUCT_TYPE);
        }

        // GET: PRODUCT_TYPE/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            if (pRODUCT_TYPE == null)
            {
                return HttpNotFound();
            }
            return View(pRODUCT_TYPE);
        }

        // POST: PRODUCT_TYPE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PRODUCT_TYPE_CD,NAME")] PRODUCT_TYPE pRODUCT_TYPE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pRODUCT_TYPE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pRODUCT_TYPE);
        }

        // GET: PRODUCT_TYPE/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            if (pRODUCT_TYPE == null)
            {
                return HttpNotFound();
            }
            return View(pRODUCT_TYPE);
        }

        // POST: PRODUCT_TYPE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            PRODUCT_TYPE pRODUCT_TYPE = db.PRODUCT_TYPE.Find(id);
            db.PRODUCT_TYPE.Remove(pRODUCT_TYPE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
