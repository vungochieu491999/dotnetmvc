﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartManagement.Models;

namespace CartManagement.Controllers
{
    public class ACCOUNTsController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();

        // GET: ACCOUNTs
        public ActionResult Index()
        {
            var aCCOUNTs = db.ACCOUNTs.Include(a => a.BRANCH).Include(a => a.CUSTOMER).Include(a => a.EMPLOYEE).Include(a => a.PRODUCT);
            return View(aCCOUNTs.ToList());
        }

        // GET: ACCOUNTs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT aCCOUNT = db.ACCOUNTs.Find(id);
            if (aCCOUNT == null)
            {
                return HttpNotFound();
            }
            return View(aCCOUNT);
        }

        // GET: ACCOUNTs/Create
        public ActionResult Create()
        {
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCHes, "BRANCH_ID", "ADDRESS");
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS");
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEEs, "EMP_ID", "FIRST_NAME");
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCTs, "PRODUCT_CD", "NAME");
            return View();
        }

        // POST: ACCOUNTs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ACCOUNT_ID,AVAIL_BALANCE,CLOSE_DATE,LAST_ACTIVITY_DATE,OPEN_DATE,PENDING_BALANCE,STATUS,CUST_ID,OPEN_BRANCH_ID,OPEN_EMP_ID,PRODUCT_CD")] ACCOUNT aCCOUNT)
        {
            if (ModelState.IsValid)
            {
                db.ACCOUNTs.Add(aCCOUNT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCHes, "BRANCH_ID", "ADDRESS", aCCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", aCCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEEs, "EMP_ID", "FIRST_NAME", aCCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCTs, "PRODUCT_CD", "NAME", aCCOUNT.PRODUCT_CD);
            return View(aCCOUNT);
        }

        // GET: ACCOUNTs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT aCCOUNT = db.ACCOUNTs.Find(id);
            if (aCCOUNT == null)
            {
                return HttpNotFound();
            }
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCHes, "BRANCH_ID", "ADDRESS", aCCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", aCCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEEs, "EMP_ID", "FIRST_NAME", aCCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCTs, "PRODUCT_CD", "NAME", aCCOUNT.PRODUCT_CD);
            return View(aCCOUNT);
        }

        // POST: ACCOUNTs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ACCOUNT_ID,AVAIL_BALANCE,CLOSE_DATE,LAST_ACTIVITY_DATE,OPEN_DATE,PENDING_BALANCE,STATUS,CUST_ID,OPEN_BRANCH_ID,OPEN_EMP_ID,PRODUCT_CD")] ACCOUNT aCCOUNT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aCCOUNT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OPEN_BRANCH_ID = new SelectList(db.BRANCHes, "BRANCH_ID", "ADDRESS", aCCOUNT.OPEN_BRANCH_ID);
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", aCCOUNT.CUST_ID);
            ViewBag.OPEN_EMP_ID = new SelectList(db.EMPLOYEEs, "EMP_ID", "FIRST_NAME", aCCOUNT.OPEN_EMP_ID);
            ViewBag.PRODUCT_CD = new SelectList(db.PRODUCTs, "PRODUCT_CD", "NAME", aCCOUNT.PRODUCT_CD);
            return View(aCCOUNT);
        }

        // GET: ACCOUNTs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ACCOUNT aCCOUNT = db.ACCOUNTs.Find(id);
            if (aCCOUNT == null)
            {
                return HttpNotFound();
            }
            return View(aCCOUNT);
        }

        // POST: ACCOUNTs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ACCOUNT aCCOUNT = db.ACCOUNTs.Find(id);
            db.ACCOUNTs.Remove(aCCOUNT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
