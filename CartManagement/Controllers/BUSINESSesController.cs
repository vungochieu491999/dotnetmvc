﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartManagement.Models;

namespace CartManagement.Controllers
{
    public class BUSINESSesController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();

        // GET: BUSINESSes
        public ActionResult Index()
        {
            var bUSINESSes = db.BUSINESSes.Include(b => b.CUSTOMER);
            return View(bUSINESSes.ToList());
        }

        // GET: BUSINESSes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BUSINESS bUSINESS = db.BUSINESSes.Find(id);
            if (bUSINESS == null)
            {
                return HttpNotFound();
            }
            return View(bUSINESS);
        }

        // GET: BUSINESSes/Create
        public ActionResult Create()
        {
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS");
            return View();
        }

        // POST: BUSINESSes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "INCORP_DATE,NAME,STATE_ID,CUST_ID")] BUSINESS bUSINESS)
        {
            if (ModelState.IsValid)
            {
                db.BUSINESSes.Add(bUSINESS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", bUSINESS.CUST_ID);
            return View(bUSINESS);
        }

        // GET: BUSINESSes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BUSINESS bUSINESS = db.BUSINESSes.Find(id);
            if (bUSINESS == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", bUSINESS.CUST_ID);
            return View(bUSINESS);
        }

        // POST: BUSINESSes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "INCORP_DATE,NAME,STATE_ID,CUST_ID")] BUSINESS bUSINESS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bUSINESS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", bUSINESS.CUST_ID);
            return View(bUSINESS);
        }

        // GET: BUSINESSes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BUSINESS bUSINESS = db.BUSINESSes.Find(id);
            if (bUSINESS == null)
            {
                return HttpNotFound();
            }
            return View(bUSINESS);
        }

        // POST: BUSINESSes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BUSINESS bUSINESS = db.BUSINESSes.Find(id);
            db.BUSINESSes.Remove(bUSINESS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
