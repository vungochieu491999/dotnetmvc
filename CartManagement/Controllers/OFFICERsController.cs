﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CartManagement.Models;

namespace CartManagement.Controllers
{
    public class OFFICERsController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();

        // GET: OFFICERs
        public ActionResult Index()
        {
            var oFFICERs = db.OFFICERs.Include(o => o.CUSTOMER);
            return View(oFFICERs.ToList());
        }

        // GET: OFFICERs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFICER oFFICER = db.OFFICERs.Find(id);
            if (oFFICER == null)
            {
                return HttpNotFound();
            }
            return View(oFFICER);
        }

        // GET: OFFICERs/Create
        public ActionResult Create()
        {
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS");
            return View();
        }

        // POST: OFFICERs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OFFICER_ID,END_DATE,FIRST_NAME,LAST_NAME,START_DATE,TITLE,CUST_ID")] OFFICER oFFICER)
        {
            if (ModelState.IsValid)
            {
                db.OFFICERs.Add(oFFICER);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", oFFICER.CUST_ID);
            return View(oFFICER);
        }

        // GET: OFFICERs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFICER oFFICER = db.OFFICERs.Find(id);
            if (oFFICER == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", oFFICER.CUST_ID);
            return View(oFFICER);
        }

        // POST: OFFICERs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OFFICER_ID,END_DATE,FIRST_NAME,LAST_NAME,START_DATE,TITLE,CUST_ID")] OFFICER oFFICER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oFFICER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", oFFICER.CUST_ID);
            return View(oFFICER);
        }

        // GET: OFFICERs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OFFICER oFFICER = db.OFFICERs.Find(id);
            if (oFFICER == null)
            {
                return HttpNotFound();
            }
            return View(oFFICER);
        }

        // POST: OFFICERs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OFFICER oFFICER = db.OFFICERs.Find(id);
            db.OFFICERs.Remove(oFFICER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
